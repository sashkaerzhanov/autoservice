package OOP.delegation;


import OOP.Vehicle;

/**
 * Базовый класс для машин
 * @author Alexander Erzhanov
 * @since 18.07.2017
 */
public class Car implements Vehicle {

    /**
     * Марка,Модель,Год выпуска
     */
    private final String carMake;
    private final String carModel;
    private final int carYear;


    /**
     * Конструктор
     * @param carMake  Марку машины
     * @param carModel Модель машины
     * @param carYear  Год выпуска
     */
    public Car(String carMake, final String carModel, int carYear) {
        this.carMake = carMake;
        this.carModel = carModel;
        this.carYear = carYear;
    }



    /**
     * Возвращает марку транспортного средства
     */
    @Override
    public String getVehicleMake() {
        return carMake;
    }

    /**
     * Возвращает модель транспортного средства
     */
    @Override
    public String getVehicleModel() {
        return carModel;
    }

    /**
     * Возвращает год выпуска транспортного средства
     */
    @Override
    public int getVehicleYear() {
        return carYear;
    }

    /**
     * @return hashCode
     */
    @Override
    public int hashCode() {
        int result = carMake != null ? carMake.hashCode() : 0;
        result = 31 * result + carYear;
        return result;
    }


    /**
     * @return true, если эквивалентны
     */
    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Car that = (Car) o;

        if (this.carYear != that.carYear) {
            return false;
        }
        if (this.carMake == null) {
            return that.carMake == null;
        } else {
            return this.carMake.equals(that.carMake);
        }
    }

    /**
     * @return str
     */
    @Override
    public String toString() {
        return "Car{" +
                "carMake='" + carMake + '\'' +
                ", carModel='" + carModel + '\'' +
                ", carYear=" + carYear +
                '}';
    }


}