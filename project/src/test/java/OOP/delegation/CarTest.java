package OOP.delegation;

import org.junit.Assert;
import org.junit.Test;

/**
 * Тесты для класса Car
 * @author Alexander Erzhanov
 * @since 28.07.2017
 */
public class CarTest {
    Car car = new Car("Porshe","911",1996);
    @Test
    public void testGetVehicleMake() throws Exception {
        Assert.assertEquals("Porshe",car.getVehicleMake());

    }

    @Test
    public void testGetVehicleModel() throws Exception {
        Assert.assertEquals("911",car.getVehicleModel());
    }

    @Test
    public void TestGetVehicleYear() throws Exception {
        Assert.assertEquals(1996,car.getVehicleYear());
    }

    @Test
    public void testHashCode() throws Exception {
        car.hashCode();
        Assert.assertEquals(1272498639,car.hashCode());

    }

    @Test
    public void testEquals() throws Exception {
        Car car2 = new Car("Porshe","911",1996);
        Assert.assertEquals(true, car2.equals(car));
    }

    @Test
    public void testToString() throws Exception {
        Assert.assertEquals("Car{carMake='Porshe', carModel='911', carYear=1996}", car.toString());
    }

}