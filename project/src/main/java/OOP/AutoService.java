package OOP;

import OOP.delegation.Car;

import java.util.Arrays;

/**
 * Класс описывает автосервис
 * @author Alexander Erzhanov
 * @since 20.07.2017
 */
public class AutoService {
    /**
     * Список клиентов
     */
    private final Client [] clients;

    /**
     * @return Список клиентов
     */
    public Client[] getClients() {
        return clients;
    }

    /**
     * Конструктор для автосервиса
     * @param size
     */
    public AutoService(final int size) {
        this.clients = new Client[size];
    }


    /**
     * Добавить клиента
     * @param id
     * @param client
     */
    public void addClient (final int id, final Client client) {
        this.clients[id] = client;
    }

    /**
     * Изменить клиента
     * @param id
     * @param newClient
     */
    public void editClient (final int id, final Client newClient) {
        this.clients[id] = newClient;
    }

    /**
     * Удалить клиента
     * @param name
     */
    public void deleteClient (final String name) {
        for (int i = 0; i < clients.length; i++) {
            if (clients[i].getClientName().compareTo(name)== 0) {
                clients[i] = new Client("-", new Car("-","-",0));
                break;

            }
        }
    }

    /**
     * Находит свободное место в сервисе
     * @return Свободное место
     * @throws Exception, если сервис заполнен
     */
    public int lookForFree() {
        int index = 0;
        for(int i = 0; i<clients.length; i++){
            if(clients[i] == null){
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * @return str
     */
    @Override
    public String toString() {
        return "AutoService{" +
                "clients=" + Arrays.toString(clients) +
                '}';
    }

    /**
     * Найти клиента по машине
     * @param car
     * @return client
     */
    public Client findClientByCar(Car car)throws Exception{
        Client result = null ;
        for (int i=0; i< clients.length; i++){
            if (clients[i].getCar().equals(car)){
               result = clients[i];
               break;
            }
            else result = new Client("-", new Car("-","-",0));
        }
        return result;
    }

    /**
     * Найти клиента по машине
     * @param name
     * @return client
     */
    public Client findClientByName (String name) throws Exception {
        Client result = null ;
        for (int i=0; i< clients.length; i++){
            if (clients[i].getClientName().compareTo(name)== 0) {
                result = clients[i];
                break;
            }
            else result = new Client("-", new Car("-","-",0));
        }
        return result;
    }

}
