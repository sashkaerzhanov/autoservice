package IO;

/**
 * Интерфейс вывода
 * @author Alexander Erzhanov
 * @since 05.08.2017
 */
public interface Output {
    /**
     * Вывод строки на экран
     * @param line
     */
    void println(String line);
}
