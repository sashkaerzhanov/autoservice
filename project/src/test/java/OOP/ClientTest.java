package OOP;

import OOP.delegation.Car;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Тесты для класса Client
 * @author Alexander Erzhanov
 * @since 28.07.2017
 */
public class ClientTest {
    private Client client = new Client("Bob",new Car("Mercedes","S-class",2017));
    @Test
    public void testGetClientName() throws Exception {
        assertEquals("Bob",client.getClientName());
    }

    @Test
    public void testGetCar() throws Exception {
        assertEquals("Bob",client.getClientName());
    }


}