package OOP.inheritance;

/**
 * Базовый класс для гражданской авиации
 * @author Alexander Erzhanov
 * @since 18.07.2017
 */
public class PassengerPlane extends Plane {
    private final String planeType = "Passenger";
    /**
     * Конструктор
     * @param planeMake  Маркf самолета
     * @param planeModel Модель самолета
     * @param planeYear  Год выпуска
     */
    public PassengerPlane(String planeMake, String planeModel, int planeYear) {
        super(planeMake, planeModel, planeYear);

    }

    /**
     * Возвращает тип самолета
     * @return
     */
    public String getPlaneType() {
        return planeType;
    }

}
