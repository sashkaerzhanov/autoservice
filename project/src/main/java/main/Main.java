package main;

import IO.Input;
import IO.MyInput;
import IO.MyOutput;
import IO.Output;
import OOP.AutoService;
import OOP.ServiceRunner;

import java.util.Scanner;



/**
 * @author Alexander Erzhanov
 * @since 19.07.2017
 */
public class Main {

    public static void main(String [] arg)throws Exception{
        final Output out = new MyOutput();
        Scanner scanner = new Scanner(System.in);
        final Input in = new MyInput(scanner,out);
        AutoService service = new AutoService(10);
        ServiceRunner runner = new ServiceRunner(out,service,in);
        runner.activateService();
    }



}