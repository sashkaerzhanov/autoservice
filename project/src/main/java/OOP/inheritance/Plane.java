package OOP.inheritance;

import OOP.Vehicle;

/**
 * Базовый класс для самолетов
 * @author Alexander Erzhanov
 * @since 18.07.2017
 */
public class Plane implements Vehicle {

    /**
     * Марка,Модель,Год выпуска
     */
    private final String planeMake;
    private final String planeModel;
    private final int planeYear;


    /**
     * Конструктор
     * @param planeMake  Марку самолета
     * @param planeModel Модель самолета
     * @param planeYear  Год выпуска
     */
    public Plane(String planeMake, String planeModel, int planeYear) {
        this.planeMake = planeMake;
        this.planeModel = planeModel;
        this.planeYear = planeYear;
    }

    /**
     * Возвращает марку транспортного средства
     */
    @Override
    public String getVehicleMake() {
        return planeMake;
    }

    /**
     * Возвращает модель транспортного средства
     */
    @Override
    public String getVehicleModel() {
        return planeModel;
    }

    /**
     * Возвращает год выпуска транспортного средства
     */
    @Override
    public int getVehicleYear() {
        return planeYear;
    }
}