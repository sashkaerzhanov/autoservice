package IO;

/**
 * @author Alexander Erzhanov
 * @since 05.08.2017
 */
public class MyOutput implements Output {

    public void println(String line) {
        System.out.println(line);
    }
}
