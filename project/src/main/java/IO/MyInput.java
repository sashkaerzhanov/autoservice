package IO;

import java.util.Scanner;

/**
 * @author Alexander Erzhanov
 * @since 05.08.2017
 */
final public class MyInput implements Input {
    Output output;
    Scanner scanner;

    /**
     * Конструктор
     * @param scanner
     * @param output
     */
    public MyInput(Scanner scanner, Output output){
        this.output = output;
        this.scanner = scanner;
    }
    public String next() {
        return this.scanner.next();
    }

    public String ask(String question) {
        this.output.println(question);
        return this.scanner.next();
    }

    public void close() {

    }
}
