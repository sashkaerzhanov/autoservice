package OOP;

import OOP.delegation.Car;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Тесты для класса AutoService
 * @author Alexander Erzhanov
 * @since 28.07.2017
 */
public class AutoServiceTest {


    @Test
    public void testEditClient() throws Exception {
        AutoService service = new AutoService(1);
        Client client = new Client("Alex",new Car("BMW","M5",2017));
        Client newClient = new Client("Mike",new Car("Subaru","Forester",2006));
        service.addClient(0,client);
        service.editClient(0,newClient);
        assertEquals(service.getClients()[0],newClient);
    }

    @Test
    public void testDeleteClient() throws Exception {
        AutoService service = new AutoService(1);
        Client client = new Client("Alex",new Car("BMW","M5",2017));
        service.addClient(0,client);
        service.deleteClient("Alex");
        assertEquals("-",service.getClients()[0].getClientName());
    }

    @Test
    public void tstFindClientByCar() throws Exception {
        AutoService service = new AutoService(1);
        Client client = new Client("Alex",new Car("BMW","M5",2017));
        service.addClient(0,client);
        assertNotNull(service.findClientByCar(new Car("BMW","M5",2017)));
    }

    @Test
    public void testFindClientByName() throws Exception {
        AutoService service = new AutoService(1);
        Client client = new Client("Alex",new Car("BMW","M5",2017));
        service.addClient(0,client);
        assertNotNull(service.findClientByName("Alex"));
    }

}