package OOP.delegation;

import OOP.Vehicle;

/**
 * Базовый класс для спортивных машин
 * @author Alexander Erzhanov
 * @since 19.07.2017
 */
public class SportCar implements Vehicle {

    private final Vehicle vehicle;

    public SportCar(final Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * Возвращает марку транспортного средства
     */
    @Override
    public String getVehicleMake() {
        return this.vehicle.getVehicleMake();
    }

    /**
     * Возвращает модель транспортного средства
     */
    @Override
    public String getVehicleModel() {
        return this.vehicle.getVehicleModel();
    }

    /**
     * Возвращает год выпуска транспортного средства
     */
    @Override
    public int getVehicleYear() {
        return this.vehicle.getVehicleYear();
    }
}
