package OOP;

import IO.Input;
import IO.Output;
import OOP.delegation.Car;

/**
 * Запуск сервиса
 * @author Alexander Erzhanov
 * @since 05.08.2017
 */
public class ServiceRunner {
    private final Output output;
    private final Input input;
    private final AutoService service;

    public ServiceRunner(Output output, AutoService service, Input input) {
        this.output = output;
        this.service = service;
        this.input = input;
    }

    public void activateService() throws Exception {
        service.addClient(0,new Client("Bob",new Car("BMW","M5",2017)));
        service.addClient(1,new Client("John",new Car("Jeep","SRT",2016)));
        service.addClient(1,new Client("Alex",new Car("Jeep","SRT",2016)));
        boolean active = true;
        while(active) {
            output.println("\nENTER THE NUMBER OF OPERATION: ");
            output.println("[1] - LIST;                         "+
                    "\n[2] - ADD CLIENT;                   [6] - CHANGE CLIENT;"+
                    "\n[3] - SEARCH BY CLIENT NAME;        " +
                    "\n[4] - SEARCH BY CAR;                [7] - QUIT."+
                    "\n[5] - DELETE CLIENT; ");
            try {
                int number = Integer.valueOf(input.next());
                switch (number) {
                    case 1:
                        output.println("List: ");
                        writeList();
                        break;
                    case 2:
                        int index = service.lookForFree();
                        output.println("Registration ");

                            System.out.println("Enter name : ");
                            /** user input name */
                            String name = input.next();
                            System.out.println("Enter car make : ");
                            /** user input make */
                            String carMake = input.next();
                            System.out.println("Enter car model");
                            /** user input car Model */
                            String carModel = input.next();
                            System.out.println("Enter car year");
                            /** user input car Year */
                            int carYear = Integer.valueOf(input.next().trim());
                            if(carYear < 1500) {
                                throw new IllegalArgumentException("Please enter a correct year!!!");
                            }
                            service.addClient(index, new Client(name, new Car(carMake, carModel, carYear)));
                        output.println("List ");
                        writeList();
                        break;

                    case 3:
                            Client findbyName = service.findClientByName(input.ask("Enter client name for searching:"));
                            write(findbyName);
                        break;

                    case 4:
                        try {
                            System.out.println("Enter CAR for searching:");
                            /** user input make */
                            String carMake1 = input.ask("Enter car make : ");
                            /** user input car Model */
                            String carModel1 = input.ask("Enter car model");
                            System.out.println("Enter car year");
                            /** user input car Year */
                            int carYear1 = Integer.valueOf(input.next().trim());
                            if(carYear1 < 1500) {
                                throw new IllegalArgumentException("Please enter a correct year!!!");
                            }
                            Client findbyCAR = service.findClientByCar(new Car(carMake1, carModel1, carYear1));
                            write(findbyCAR);
                        } catch (NullPointerException e) {
                            output.println("Has no Client with the same car!");
                        }
                        break;

                    case 5:
                        service.deleteClient(input.ask("Enter client name for deleting:"));
                        output.println("List: ");
                        writeList();
                        break;


                    case 6:
                        try {
                            int id = Integer.valueOf(input.ask("Enter index of client:"));
                            System.out.println("Enter name : ");
                            /** user input name */
                            String newName = input.next();
                            System.out.println("Enter car make : ");
                            /** user input make */
                            String newCarMake = input.next();
                            System.out.println("Enter car model");
                            /** user input car Model */
                            String newCarModel = input.next();
                            System.out.println("Enter car year");
                            /** user input car Year */
                            int newCarYear = Integer.valueOf(input.next().trim());
                            if(newCarYear < 1500) {
                                throw new IllegalArgumentException("Please enter a correct year!!!");
                            }
                            service.editClient(id, new Client(newName, new Car(newCarMake, newCarModel, newCarYear)));
                            output.println("List: ");
                            writeList();
                        }catch (NullPointerException e){
                            output.println("Has no client with the same index");
                        }
                        break;


                    case 7:
                        active = false;
                        break;
                    default:
                        output.println("Entered incorrect data!");
                }
            } catch (NumberFormatException e) {
                output.println("Input number!");
            }

        }
    }

    public void write(Client cl){
        output.println("ClientName :"+ cl.getClientName());
        output.println("CAR :"+ cl.getCar());
    }
    public void writeList(){
        int len = service.lookForFree();
        int i = 0;
        Client c;
        while(i < len){
            c = service.getClients()[i];
            output.println(c.getClientName()+"  "+c.getCar()+"  ");
            i++;
        }
    }
}
