package OOP;

/**
 * Интерфейс описывающий транспортное средство
 * @author Alexander Erzhanov
 * @since 18.07.2017
 */
public interface Vehicle {

    /**
     * Возвращает марку транспортного средства
     */
    String getVehicleMake();
    /**
     * Возвращает модель транспортного средства
     */
    String getVehicleModel();
    /**
     * Возвращает год выпуска транспортного средства
     */
    int getVehicleYear();


}

