package OOP;

import OOP.delegation.Car;

/**
 * Базовый класс для клиентов
 * @author Alexander Erzhanov
 * @since 20.07.2017
 */
public class Client {
    private final String clientName;
    private final Car car;

    /**
     * Конструктор
     * @param clientName
     * @param car
     */
    public Client(String clientName, Car car) {
        this.clientName = clientName;
        this.car = car;
    }

    /**
     * @return Имя клиента
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * @return Машину
     */
    public Car getCar() {
        return car;
    }


    /**
     * @return str
     */
    @Override
    public String toString() {
        return "Client{" +
                "clientName='" + clientName + '\'' +
                ", car=" + car +
                '}';
    }
}
