package IO;

/**
 * Интерфейс ввода
 * @author Alexander Erzhanov
 * @since 05.08.2017
 */
public interface Input {
    /**
     * @return следующий элемент
     */
    String next();

    /**
     * @param question
     * @return следующий элемент
     */
    String ask(String question);


    void close();
}
